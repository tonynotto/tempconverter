# Temp Converter

This python app takes in an input temperature, a target unit of measure, anda student’s numeric response. It then compares whether the student’s response matches an authoritative answer after both the student’s response and authoritative answer are rounded to the ones place, and indicates that the response is correct, incorrect, or invalid.

### Requirements
This program requires python 3 or higher.

### Installing
Copy tempconvert.py to the loaction from which you would like to run it.

### Usage
Call python and provide this program's filename and argurments in order:
`/usr/bin/python tempconvert.py <input temperature value> <input temperature unit> <target unit> <student response>`

For example:
```
$ /usr/bin/python tempconvert.py 84.2 Fahrenheit Rankine 543.5
correct
```