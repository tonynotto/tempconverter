import sys


class TempConverter:
    """
    Class to convert temperature between different units.
    """

    def convert(self, in_value, in_unit, out_unit):
        """Convert between different temperature units."""
        # Let's use "kelvin" as a common intermediary to simplify
        lowerin = in_unit.lower()
        if lowerin == "kelvin":
            value = in_value
        elif lowerin == "rankine":
            value = in_value * (5/9)
        elif lowerin == "celsius":
            value = in_value + 273.15
        else:  # fahrenheit
                value = (in_value + 459.67) * (5/9)

        lowerout = out_unit.lower()
        if lowerout == "kelvin":
                converted = value
        elif lowerout == "rankine":
                converted = value * (9/5)
        elif lowerout == "celsius":
                converted = value - 273.15
        else:  # fahrenheit
                converted = value * (9/5) - 459.67

        return converted


def validate_unit(unit):
    """Check whether the provided unit type is valid"""
    if type(unit) is str and \
            unit.lower() == "kelvin" or \
            unit.lower() == "rankine" or \
            unit.lower() == "celsius" or \
            unit.lower() == "fahrenheit":
        return True
    else:
        print(unit)
        print(unit.lower())
        print(type(unit))
        return False


def main(input_temp_val, input_temp_unit, output_unit, student_response):
    """Take in the input temperature value and unit, output unit, and student
        response, and check for correctness"""

    if not validate_unit(input_temp_unit) or not validate_unit(output_unit):
        print("invalid unit")
        exit()

    try:
        val = float(input_temp_val)
    except ValueError:
        print("invalid input temperature value")
        exit()
    try:
        resp = float(student_response)
    except ValueError:
        print("incorrect")
        exit()

    converter = TempConverter()
    con_value = converter.convert(val, input_temp_unit, output_unit)

    if round(con_value) == round(resp):
        print("correct")
    else:
        print("incorrect")


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
