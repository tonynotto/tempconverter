import tempconvert


def test_convert():
    """Test the TempConverter convert method"""
    tc = tempconvert.TempConverter()
    assert round(tc.convert(84.2, "Fahrenheit", "Rankine")) == 544


def test_all(capfd):
    """Test calling the main method of the script"""
    tempconvert.main("-45.14", "Celsius", "Kelvin", "227.51")
    out, err = capfd.readouterr()
    assert out == "correct\n"


def test_incorrect(capfd):
    """Test an incorrect student response"""
    tempconvert.main("-45.14", "Celsius", "Kelvin", "999")
    out, err = capfd.readouterr()
    assert out == "incorrect\n"
